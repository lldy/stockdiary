package com.cch.test.generic;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class GericTest<T> {
	
	@SuppressWarnings("unused")
	protected Class<T> entityClass;

	@SuppressWarnings("unchecked")
	public GericTest(){
		Type genType = getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		entityClass = (Class) params[0];
	}
	
	public static void main(String[] args) {
		new GericTest<String>();
	}
}
