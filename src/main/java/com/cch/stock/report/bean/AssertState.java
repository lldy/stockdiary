package com.cch.stock.report.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cch.platform.base.bean.BaseUser;
import com.cch.platform.dao.HibernateDao;
import com.cch.stock.bill.bean.SdBill;

public class AssertState {
	
	protected BaseUser user;
	
	//
	protected Date beginTime;
	
	protected Date nowTime;
	
	//总资产
	protected double allAssert=0;
	
	//总成本
	protected double allCost=0;
	
	//盈利
	protected double profit=0;
	
	//可用资金
	protected double money=0;
	
	//市值
	protected double stockValue=0;
	
	//持有股票
	protected Map<String,Integer> stocks=new HashMap<String,Integer>();
	
	public void addBill(SdBill bill) {
		this.money +=  bill.getBillMoney();
		if("BSTRANS".equals(bill.getBillBigcatory())){
			this.allCost += bill.getBillMoney();
		}
		if(bill.getStockCnt()!=null){
			Integer cnt=stocks.get(bill.getStockCode());
			if(cnt!=null)cnt+=bill.getStockCnt();
			else cnt=bill.getStockCnt();
			if(cnt==0){
				stocks.remove(bill.getStockCode());
			}else{
				stocks.put(bill.getStockCode(), cnt);
			}
		}
	}
	
	public void updateStockValue(double stockValue) {
		this.stockValue = stockValue;
		this.allAssert=this.money+stockValue;
		this.profit=this.allAssert-this.allCost;
	}
	
	public Map<String,List<Object>> getDataArray(){
		Map<String,List<Object>> re=new HashMap<String,List<Object>>();
		re.put("nowTime", new ArrayList<Object>());
		re.put("allAssert",new ArrayList<Object>());
		re.put("allCost",new ArrayList<Object>());
		re.put("profit",new ArrayList<Object>());
		re.put("stockValue",new ArrayList<Object>());
		return this.getDataArray(re);
	}
	
	public Map<String,List<Object>> getDataArray(Map<String,List<Object>> re){
		re.get("nowTime").add(this.getNowTime());
		re.get("allAssert").add(this.getAllAssert());
		re.get("allCost").add(this.getAllCost());
		re.get("profit").add(this.getProfit());
		re.get("stockValue").add(this.getStockValue());
		return re;
	}
	
	public BaseUser getUser() {
		return user;
	}

	public void setUser(BaseUser user) {
		this.user = user;
	}

	public Map<String, Integer> getStocks() {
		return stocks;
	}

	public void setStocks(Map<String, Integer> stocks) {
		this.stocks = stocks;
	}

	public double getAllAssert() {
		return allAssert;
	}

	public void setAllAssert(double allAssert) {
		this.allAssert = allAssert;
	}

	public double getAllCost() {
		return allCost;
	}

	public void setAllCost(double allCost) {
		this.allCost = allCost;
	}
	
	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public double getStockValue() {
		return stockValue;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getNowTime() {
		return nowTime;
	}

	public void setNowTime(Date nowTime) {
		this.nowTime = nowTime;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}
}
