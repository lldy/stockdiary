package com.cch.stock.bill.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * SdTradeType entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sd_trade_type")
public class SdTradeType implements java.io.Serializable {

	// Fields

	private String tradeCode;
	private String tradeName;
	private String tradePcode;

	// Constructors

	/** default constructor */
	public SdTradeType() {
	}

	/** minimal constructor */
	public SdTradeType(String tradeCode, String tradeName) {
		this.tradeCode = tradeCode;
		this.tradeName = tradeName;
	}

	/** full constructor */
	public SdTradeType(String tradeCode, String tradeName, String tradePcode) {
		this.tradeCode = tradeCode;
		this.tradeName = tradeName;
		this.tradePcode = tradePcode;
	}

	// Property accessors
	@Id
	@Column(name = "trade_code", unique = true, nullable = false, length = 100)
	public String getTradeCode() {
		return this.tradeCode;
	}

	public void setTradeCode(String tradeCode) {
		this.tradeCode = tradeCode;
	}

	@Column(name = "trade_name", nullable = false, length = 100)
	public String getTradeName() {
		return this.tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	@Column(name = "trade_pcode", length = 100)
	public String getTradePcode() {
		return this.tradePcode;
	}

	public void setTradePcode(String tradePcode) {
		this.tradePcode = tradePcode;
	}

}